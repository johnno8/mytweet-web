# README #

MyTweet is a dymanic microblogging web app developed in node.js as part of my Enterprise Web Development module. The app was developed incrementally with improved functionality added over time and uses a mongo database hosted by mLab. The live app is viewable at [https://peaceful-thicket-11060.herokuapp.com/](https://peaceful-thicket-11060.herokuapp.com/)

## Valid Logins ##
### Administrator ###
    email: admin@mytweet.ie
    password: secret

### User ###
    email: homer@simpson.com
    password: secret