/**
 * Created by John on 01/11/2016.
 */
'use strict'

const User = require('../models/user');
const Tweet = require('../models/tweet');

// Renders allusers view populated with all registered users
exports.allUsers = {

  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    let userId = null;

    User.find({}).then(allUsers => {
      reply.view('allusers', {
        title: 'All users',
        users: allUsers,
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};

// Renders the public timeline of another user
exports.publicTimeline = {

  handler: function (request, reply) {
    let id = request.params.id;

    User.findOne({ _id: id }).then(user => {
      Tweet.find({ tweetor: user }).then(usersTweets => {
        reply.view('otherusertimeline', {
          title: 'Public timeline',
          tweets: usersTweets,
          user: user,
        });
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};
