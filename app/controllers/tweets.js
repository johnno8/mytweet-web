/**
 * Created by John on 18/10/2016.
 */
'use strict'

const Tweet = require('../models/tweet');
const User = require('../models/user');
const Joi = require('joi');

// Renders users home page populated with a list of users tweets and their total number
exports.home = {

  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    let userId = null;

    User.findOne({ email: userEmail }).then(user => {
      Tweet.find({ tweetor: user }).then(usersTweets => {
        reply.view('posttweet', {
          title: 'Post a tweet',
          tweets: usersTweets.reverse(),
          user: user,
          numtweets: usersTweets.length,
        });
      }).catch(err => {
        reply.redirect('/');
      });
    });
  },
};

// Recieves text of a tweet from content form, creates a datestring and then creates a
// new tweet object with those values
exports.postATweet = {

  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;
    let userId = null;
    let tweet = null;
    let d = new Date();
    let datestring = ('0' + d.getDate()).slice(-2) + '-' + ('0' + (d.getMonth() + 1)).slice(-2) +
        '-' + d.getFullYear() + ' ' + ('0' + d.getHours()).slice(-2) + ':' +
        ('0' + d.getMinutes()).slice(-2);

    User.findOne({ email: userEmail }).then(user => {
      let data = request.payload;
      userId = user._id;
      tweet = new Tweet(data);
      tweet.tweetor = userId;
      tweet.date = datestring;
      tweet.save();
    }).then(newTweet => {
      reply.redirect('/home');
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

// Renders timeline view with all tweets from all users displayed
exports.timeline = {

  handler: function (request, reply) {
    Tweet.find({}).populate('tweetor').then(allTweets => {
      reply.view('timeline', {
        title: 'All user\'s tweets',
        tweets: allTweets.reverse(),
      });
    }).catch(err => {
      reply.redirect('/');
    });
  },
};

// Allow a user to delete a single tweet of their own
exports.deleteTweet = {

  handler: function (request, reply) {
    let id = request.params.id;

    Tweet.findOneAndRemove({ _id: id }, function (error) {
      if (error) {
        reply({
          statusCode: 503,
          message: 'Error removing tweet',
        });
      } else {
        reply.redirect('/home');
      }
    });
  },
};

// Allows a user to delete all their own tweets
exports.deleteAll = {

  handler: function (request, reply) {
    let userEmail = request.auth.credentials.loggedInUser;

    User.findOne({ email: userEmail }).then(user => {
      Tweet.find({ tweetor: user }).then(usersTweets => {
        usersTweets.forEach(function (tweetid) {
          Tweet.findByIdAndRemove(tweetid, function (error) {
            if (error) {
              throw error;
            }
          });
        });
        return null;
      });
    }).then(whatever => {
      reply.redirect('/home');
    }).catch(err => {
      reply.redirect('/posttweet');
    });
  },
};

