/**
 * Created by John on 19/10/2016.
 */
const mongoose = require('mongoose');

const tweetSchema = mongoose.Schema({
  content: String,
  tweetor: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  },
  date: String,
});

const Tweet = mongoose.model('Tweet', tweetSchema);
module.exports = Tweet;
