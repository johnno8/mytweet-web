/**
 * Created by John on 19/10/2016.
 */
'use strict';

const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  regdate: String,
});

const User = mongoose.model('User', userSchema);
module.exports = User;
