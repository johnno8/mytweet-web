/**
 * Created by John on 11/10/2016.
 */
'use strict';

const Hapi = require('hapi');
require('./app/models/db');

let server = new Hapi.Server();
server.connection({ port: process.env.PORT || 4000 });

/*const initUsers = {
  'bart@simpson.com': {
    firstName: 'bart',
    lastName: 'simpson',
    email: 'bart@simpson.com',
    password: 'secret',
  },
  'homer@simpson.com': {
    firstName: 'homer',
    lastName: 'simpson',
    email: 'homer@simpson.com',
    password: 'secret',
  },
};*/

// server.bind({
//   //currentUser: {},
//   users: initUsers,
// });


server.register([require('inert'), require('vision'), require('hapi-auth-cookie')], err => {

  if (err) {
    throw err;
  }

  server.auth.strategy('standard', 'cookie', {
    password: 'secretpasswordnotrevealedtoanyone',
    cookie: 'mytweet-cookie',
    isSecure: false,
    ttl: 24 * 60 * 60 * 1000,
    redirectTo: '/login',
  });

  server.auth.default({
    strategy: 'standard',
  });

  server.views({
    engines: {
      hbs: require('handlebars'),
    },
    relativeTo: __dirname,
    path: './app/views',
    layoutPath: './app/views/layout',
    partialsPath: './app/views/partials',
    layout: true,
    isCached: false,
  });

  server.route(require('./routes'));

  server.start((err) => {
    if (err) {
      throw err;
    }

    console.log('Server listening at:', server.info.uri);
  });

});
